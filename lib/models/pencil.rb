class Pencil

  # NOTE: I made these accessors public for ease of testing, since this is a small-scoped project. If this was part of a larger system where I would be concerned about other classes overstepping their bounds, I would make these private (or, in a case where you might want something else to be able to check the status of a pencil's attributes, have public readers and private writers).
  #
  attr_accessor :max_point_durability, :point_durability, :length, :eraser_durability

  def initialize(point_durability:, eraser_durability:, length:)
    @max_point_durability = point_durability
    @point_durability = point_durability
    @eraser_durability = eraser_durability
    @length = length
  end

  def write(text, paper)
    text.chars.each do |char|
      if self.point_durability == 0
        paper.text += " "
      else
        if char.match(/[A-Z]/)
          self.point_durability -= 2
        elsif char.match(/\S/)
          self.point_durability -= 1
        end
        paper.text += char
      end
    end
  end

  def sharpen
    unless self.length == 0
      self.point_durability = self.max_point_durability
      self.length -= 1
    end
  end

  def erase(text, paper)
    erased_reversed_text = reverse_and_erase_text(text)
    paper.text = paper.text.reverse.sub(text.reverse, erased_reversed_text).reverse
  end

  def edit(text, paper)
    gap_starting_index = paper.text.index(/\s{3}/)
    return unless gap_starting_index

    current_text_index = gap_starting_index + 1

    text.chars.each do |new_char|
      existing_char = paper.text[current_text_index]
      if existing_char.match(/\s/)
        paper.text[current_text_index] = new_char
      else
        paper.text[current_text_index] = "@"
      end
      current_text_index += 1
    end
  end

  private

  def reverse_and_erase_text(text)
    text.reverse.chars.map do |char|
      if char.match(/\s/) || eraser_durability == 0
        char
      else
        self.eraser_durability -= 1
        " "
      end
    end.join("")
  end
end
