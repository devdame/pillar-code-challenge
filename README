Pencil Simulator!

This kata was written using Ruby 2.4.5, and should be compatible with any version of Ruby 2.0.

Before running the tests, you'll want to use bundler to grab its dependencies. You can do so on the command
line by running `bundle` from the root directory of the project.

If you do not have bundler installed, you can do so by running `gem install bundler`.

Once you're bundled, simply run `rspec` to run the test suite.



A few quick design notes:

- I decided to make `Paper` its own class, even though its only current function is to hold a string of its
contents. This should make the implementation much more easily expandable should we want to introduce other
paper-related behavior (eg: printing out its text) or state (eg: paper durability, size, etc).

- I considered giving `Pencil` a `@paper` attribute so you wouldn't have to pass the paper object in for
each method, but it didn't seem like paper as a concept really worked as a part of the pencil's _state_. A
pencil isn't bound to any paper, and its relationship with paper is more of one that is defined by its
individual actions. Potentially, you could have something that internally uses a `@current_paper` attribute
and would work like this:

```
pencil.on_paper(paper) do
  pencil.write("hello")
  pencil.write(" world")
end
```

or...

```
pencil.use_paper(paper)
pencil.write("hello")
pencil.write(" world")
pencil.clear_paper
```

...but this seemed like a possible extension rather than a core part of this kata. Also, `#clear_paper` is a
horribly ambiguous method name and I haven't been able to think of anything more appropriate!

Thanks for taking the time to review this Kata. :)
