require "rspec"

describe Paper do

  describe "text" do
    it "is set on initialization" do
      text = "this is what's written on the paper"
      paper = Paper.new(text)
      expect(paper.text).to eq(text)
    end

    it "defaults to an empty string if no value is given on initialization" do
      paper = Paper.new
      expect(paper.text).to eq("")
    end

    it "can be updated" do
      paper = Paper.new
      text = "new text"
      paper.text = text
      expect(paper.text).to eq(text)
    end
  end
end
