require "rspec"

describe Pencil do
  let(:pencil) { Pencil.new(point_durability: point_durability, eraser_durability: eraser_durability, length: pencil_length) }
  let(:point_durability) { 100 }
  let(:eraser_durability) { 100 }
  let(:pencil_length) { 50 }
  let(:paper) { Paper.new }

  describe "#write" do
    it "writes the given text on the given paper" do
      text = "writing with my cool new pencil!"
      pencil.write(text, paper)
      expect(paper.text).to eq(text)
    end

    it "adds new text to any current text on the paper" do
      paper.text = "existing text"
      pencil.write(" plus new text", paper)
      expect(paper.text).to eq("existing text plus new text")
    end

    it "degrades the point durability by one point per lowercase letter written" do
      pencil.write("abcd", paper)
      expect(pencil.point_durability).to eq(point_durability - 4)
    end

    it "degrades the point durability by one point per non-alphabetical letter written" do
      pencil.write("1%`|", paper)
      expect(pencil.point_durability).to eq(point_durability - 4)
    end

    it "degrades the point durability by two points per uppercase letter written" do
      pencil.write("ABCD", paper)
      expect(pencil.point_durability).to eq(point_durability - 8)
    end

    it "does not degrade the point durability for whitespace written" do
      pencil.write("a b c \nd", paper)
      expect(pencil.point_durability).to eq(point_durability - 4)
    end

    it "does not degrade max point durability" do
      pencil.write("abcd", paper)
      expect(pencil.max_point_durability).to eq(point_durability)
    end

    context "when point durability reaches zero" do
      let(:point_durability) { 2 }

      it "writes any remaining characters as whitespace" do
        pencil.write("abcd", paper)
        expect(paper.text).to eq("ab  ")
      end
    end
  end

  describe "#sharpen" do
    it "restores point durability to its maximum value" do
      pencil.point_durability = 0
      pencil.sharpen
      expect(pencil.point_durability).to eq(point_durability)
    end

    it "decreases the length of the pencil by 1" do
      pencil.sharpen
      expect(pencil.length).to eq(pencil_length - 1)
    end

    context "when the pencil's length is 0" do
      let(:pencil_length) { 0 }

      it "does not restore point durability" do
        pencil.point_durability = 0
        pencil.sharpen
        expect(pencil.point_durability).to eq(0)
      end

      it "does not further decrease the pencil's length" do
        pencil.sharpen
        expect(pencil.length).to eq(0)
      end
    end
  end

  describe "#erase" do
    before(:each) do
      paper.text = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
    end

    it "replaces the last occurrence of the given text with an equivalent number of empty spaces" do
      pencil.erase("chuck", paper)
      expect(paper.text).to eq("How much wood would a woodchuck chuck if a woodchuck could       wood?")
    end

    it "keeps replacing the current last occurrence of the given text when called multiple times" do
      2.times { pencil.erase("chuck", paper) }
      expect(paper.text).to eq("How much wood would a woodchuck chuck if a wood      could       wood?")

      2.times { pencil.erase("chuck", paper) }
      expect(paper.text).to eq("How much wood would a wood            if a wood      could       wood?")
    end

    it "does not erase anything if the given text is not present on the paper" do
      pencil.erase("charles", paper)
      expect(paper.text).to eq("How much wood would a woodchuck chuck if a woodchuck could chuck wood?")
    end

    it "decreases eraser durability by 1 per every non-whitespace character erased" do
      text = "could chuck wood?"
      non_whitespace_characters = text.length - 2
      expect { pencil.erase(text, paper) }.to change { pencil.eraser_durability }.by(-non_whitespace_characters)
    end

    it "will no longer erase once the eraser durability reaches zero" do
      pencil.eraser_durability = 3
      pencil.erase("chuck", paper)
      expect(paper.text).to eq("How much wood would a woodchuck chuck if a woodchuck could ch    wood?")
    end
  end

  describe "#edit" do
    # NOTE: I decided to go with the first instance of whitespace here (instead of last, like when erasing) since English speakers tend to think left-to-right when _writing_ text.

    before(:each) do
      paper.text = "An apple a day keeps the doctor away"
    end

    it "replaces the first whitespace gap with the given text" do
      pencil.erase("apple", paper)
      pencil.edit("onion", paper)

      expect(paper.text).to eq("An onion a day keeps the doctor away")
    end

    it "leaves any unused whitespace" do
      pencil.erase("apple", paper)
      pencil.edit("egg", paper)

      expect(paper.text).to eq("An egg   a day keeps the doctor away")
    end

    it "only edits the first available whitespace" do
      pencil.erase("doctor", paper)
      pencil.erase("apple", paper)
      pencil.edit("onion", paper)

      expect(paper.text).to eq("An onion a day keeps the        away")
    end

    it "replaces any collisions with existing non-whitespace characters with '@'" do
      pencil.erase("apple", paper)
      pencil.edit("artichoke", paper)

      expect(paper.text).to eq("An artich@k@ay keeps the doctor away")
    end

    it "does not edit the paper's text if there is no gap of at least three spaces" do
      expect { pencil.edit("artichoke", paper) }.not_to change { paper.text }
    end
  end
end
